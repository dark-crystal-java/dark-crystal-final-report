#!/bin/sh
# Requires pandoc, texlive and eisvogel latex template https://github.com/Wandmalfarbe/pandoc-latex-template
# pandoc report.md metadata.yaml -o report.pdf --from markdown --template eisvogel --listings
pandoc report.md metadata.yaml -o report.tex --from markdown --template eisvogel --listings --biblatex --bibliography=references.bib
if [[$? == 0]] ; then
  exit $?
fi
pdflatex report && biber report && pdflatex report
