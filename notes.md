From original proposal:

> #### Objective 6 - Produce a report for the Internet Freedom community addressing the social and technical applicability of sharding technologies

> In this project we focus on two use-cases for threshold-based consensus mechanisms: distributed backup and remote wipe. There are further use-cases for this technology which are relevant to the internet freedom community. For example, consensus mechanisms for the revocation and re-issuance of keys following compromise and inheritance of sensitive data by heirs following incapacitation or death.
> 
> Furthermore, since these techniques are inherently social in nature, there are social factors relating to their utility which should be discussed.
> 
> This final report on sharding technology will complement the usability report from objective 4. Together they will form the culmination of this proposal.
> 
> The production of this report will include a research component - reviewing academic literature as well as other existing software projects in this field. It will include a detailed assessment of how this technology will shift the familiar landscape of threat models - both social and technical - that developers and security trainers will need to understand when evaluating the toolkit for potential integration into their software. We will also review the challenges we encountered during the implementation phase, and offer learnings and recommendations to the Internet Freedom community based on our experience. 
> 
> As well as being professionally produced for printing, the report will include custom-created diagrams to illustrate the technologies and threat models.
> 
> Due to the nature of the tool that we are producing, and of the tools that the toolkit will be geared towards, we find it important that the report is carefully researched, written and reviewed, so as not to miscommunicate either the benefits or the potential pitfalls associated with this new technology.

- [Our early research repo](https://gitlab.com/dark-crystal-javascript/research)
- [The rebooting web of trust paper on social recovery schemes](https://gitlab.com/dark-crystal-javascript/research/-/blob/master/evaluating-social-recovery.pdf) 
