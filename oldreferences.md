
# OldReferences

- [Aumasson, Jean Phillipe, Adrian Hamelink and Omer Shlomovits, 'A Survey of ECDSA Threshold Signing', Cryptology ePrint Archive, Report 2020/1390](https://ia.cr/2020/1390)
- [Boneh, Lynn & Shacham 'Short Signatures from the Weil Pairing' - Journal of Cryptology, Sept 2004 Vol 17 Issue 4 p297-319](https://link.springer.com/article/10.1007%2Fs00145-004-0314-9)
- [Boneh, Drijvers and Neven, 'BLS Multi-Signatures With Public-Key Aggregation' 2018](https://crypto.stanford.edu/~dabo/pubs/papers/BLSmultisig.html)
- [Chaum, David; van Heyst, Eugene (1991). 'Group signatures' Advances in Cryptology — EUROCRYPT '91. Lecture Notes in Computer Science. 547. pp. 257–265.](https://link.springer.com/chapter/10.1007%2F3-540-46416-6_22)
- Shamir, Adi (1979). "How to share a secret". Communications of the ACM. 22 (11): 612–613. doi:10.1145/359168.359176.
- [Simply Secure, 'The Limits to Digital Consent', 2021](https://simplysecure.org/resources/The_Limits_to_Digital_Consent_FINAL_Oct2021.pdf)
- [Messaging Layer Security Draft IETF proposal](https://messaginglayersecurity.rocks/mls-protocol/draft-ietf-mls-protocol.html)
- [Cohn-Gordon, K., Cremers, C., Dowling, B., Garratt, L., and D. Stebila, "A Formal Security Analysis of the Signal Messaging Protocol", 2017 IEEE European Symposium on Security and Privacy (EuroS&P), DOI 10.1109/eurosp.2017.27, April 2017](https://doi.org/10.1109/eurosp.2017.27).
- [Cohn-Gordon, K., Cremers, C., Garratt, L., Millican, J., and K. Milner, "On Ends-to-Ends Encryption: Asynchronous Group Messaging with Strong Security Guarantees", 18 January 2018](https://eprint.iacr.org/2017/666.pdf) 
- [Rosario Gennaro and Steven Goldfeder. 2018. Fast Multiparty Threshold ECDSA with Fast Trustless Setup. In 2018 ACM SIGSAC Conference on Computer and Communications Security (CCS ’18), October 15–19, 2018, Toronto, ON, Canada. ACM, New York, NY, USA, 16 pages. doi:10.1145/3243734.3243859](https://dl.acm.org/doi/pdf/10.1145/3243734.3243859)
